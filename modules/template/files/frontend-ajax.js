// ajax метод для извлечения данных для формы редактирования студентов
$( document ).ready(function() {
    $(".editStudent").click(function(){
        var id = this.getAttribute('data-id');
        editData(id);
        $("#editStudent").modal('show');
    });
});

function editData(id) {
    $.ajax({
        url: "/student/update?id="+id,
        method: 'GET',
        success: function(data){
            console.log(data);
            $('#editStudent').html(data);
            /*for(var key in data) {
                $('input[name="Student\\[+key+\\]]').val(data[key]);
            }*/
        }});
}


$('#add_entry').click(function () { // если нажата кнопка вызова окна добавления новой записи
    $("#editStudent").modal('show');
    /*var attribute = document.getElementById('add_entry');
    if(attribute.getAttribute('data-model') === 'student') { // если класс равен студенту - присваиваем путь
        var url = '/student/create';
    }*/

    // вызываем ajax-метод
});


// ajax метод сохранения данных в бд
// с помощью всплывающего окна
// для календаря расписаний
$(".lessonForm").submit(function(e){
    e.preventDefault();
    $.ajax({
        url: "/student/update",
        method: 'POST',
        data: $('.addLessonForm').serialize(),
        //dataType: 'json',
        success: function(data){
            console.log('LOL');
            $("#addLessonForm").modal('hide');
        },
        error: function () {
          console.log('Error');
        }
    });
});