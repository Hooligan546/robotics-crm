<?php

namespace app\controllers;

use app\models\Office;

class OfficeController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $offices = Office::find()->all();

        return $this->render('index', [
            'offices' => $offices,
        ]);
    }

}
