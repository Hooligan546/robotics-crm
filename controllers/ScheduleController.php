<?php

namespace app\controllers;

use app\helpers\CalendarHelper;
use app\models\Group;
use app\models\Lesson;

class ScheduleController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $lessons = Lesson::find()->select('date, name')->asArray()->all();
        $groups = Group::find()->all();
        $lessons_json = CalendarHelper::get_json($lessons, 'date', 'name');
        $model = new Lesson(); // Lesson

        return $this->render('index', [
            'lessons_json' => $lessons_json,
            'model' => $model,
            'groups' => $groups
        ]);
    }

}