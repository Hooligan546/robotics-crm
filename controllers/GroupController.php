<?php

namespace app\controllers;

use Yii;
use app\models\Group;

class GroupController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Group();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Group();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

}
