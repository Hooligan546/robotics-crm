<?php

namespace app\controllers;

use Yii;
use app\models\Lesson;

class LessonController extends \yii\web\Controller
{

    public function actions() {
        return [
            'calendar' => [
                'class' => 'understeam\calendar\CalendarAction',
                'calendar' => 'calendar',           // ID компонента календаря (да, можно подключать несколько)
                'usePjax' => true,                  // Использовать ли pjax для ajax загрузки страниц
                'widgetOptions' => [                // Опции виджета (см. CalendarWidget)
                    'clientOptions' => [            // Опции JS плагина виджета
                        //'onClick' => new JsExpression('showPopup'),   // JS функция, которая будет выполнена при клике на доступное время
                        //'onFutureClick' => new JsExpression('buyPlan'),
                        //'onPastClick' => new JsExpression('showError'),
                        // Все эти функции принимают 2 параметра: date и time
                        // Для тестирования можно использовать следующий код:
                        // 'onClick' => new JsExpression("function(d,t){alert([d,t].join(' '))}")
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Lesson models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Lesson();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Lesson model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Lesson();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            echo 'Success';
        }

    }

}
