<?php

use yii\db\Migration;

/**
 * Handles dropping parent_name from table `{{%student}}`.
 */
class m190611_130955_drop_parent_name_column_from_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('student', 'parent_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('student', 'parent_name', $this->text());
    }
}
