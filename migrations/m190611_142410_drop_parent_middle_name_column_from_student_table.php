<?php

use yii\db\Migration;

/**
 * Handles dropping parent_middle_name from table `{{%student}}`.
 */
class m190611_142410_drop_parent_middle_name_column_from_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('student', 'parent_middle_name');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('student', 'parent_middle_name', $this->text());
    }
}
