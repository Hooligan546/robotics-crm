<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%student}}`.
 */
class m190602_210925_create_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%student}}', [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'surname' => $this->text(),
            'middle_name' => $this->text(),
            'birthday' => $this->date(),
            'phone' => $this->text(),
            'parent_name' => $this->text(),
            'parent_surname' => $this->text(),
            'parent_middle_name' => $this->text(),
            'group_id' => $this->integer(),
        ]);

        $this->createIndex(
            'idx-student-group_id',
            'student',
            'group_id'
        );

        $this->addForeignKey(
            'fk-student-group_id',
            'student',
            'group_id',
            'group',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%student}}');
    }
}
