<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%group_lesson}}`.
 */
class m190605_173404_create_group_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%group_lesson}}', [
            'id' => $this->primaryKey(),
            'lesson_id' => $this->integer(),
            'group_id' => $this->integer()
        ]);


        $this->createIndex(
            'idx-group_lesson-lesson_id',
            'group_lesson',
            'lesson_id'
        );

        $this->createIndex(
            'idx-group_lesson-group_id',
            'group_lesson',
            'group_id'
        );

        $this->addForeignKey(
            'fk-group_lesson-lesson_id',
            'group_lesson',
            'lesson_id',
            'lesson',
            'id'
        );

        $this->addForeignKey(
            'fk-group_lesson-group_id',
            'group_lesson',
            'group_id',
            'group',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%group_lesson}}');
    }
}
