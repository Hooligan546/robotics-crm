<?php

use yii\db\Migration;

/**
 * Handles adding parent to table `{{%student}}`.
 */
class m190611_142616_add_parent_column_to_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('student', 'parent', $this->text()->after('phone'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('student', 'parent');
    }
}
