<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%student_lesson}}`.
 */
class m190605_174219_create_student_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%student_lesson}}', [
            'id' => $this->primaryKey(),
            'lesson_id' => $this->integer(),
            'student_id' => $this->integer(),
            'visit' => $this->tinyInteger()
        ]);

        $this->createIndex(
            'idx-student_lesson-lesson_id',
            'student_lesson',
            'lesson_id'
        );

        $this->createIndex(
            'idx-student_lesson-student_id',
            'student_lesson',
            'student_id'
        );

        $this->addForeignKey(
            'fk-student_lesson-lesson_id',
            'student_lesson',
            'lesson_id',
            'lesson',
            'id'
        );

        $this->addForeignKey(
            'fk-student_lesson-student_id',
            'student_lesson',
            'student_id',
            'student',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%student_lesson}}');
    }
}
