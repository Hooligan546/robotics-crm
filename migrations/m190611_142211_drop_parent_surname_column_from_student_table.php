<?php

use yii\db\Migration;

/**
 * Handles dropping parent_surname from table `{{%student}}`.
 */
class m190611_142211_drop_parent_surname_column_from_student_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('student', 'parent_surname');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('student', 'parent_surname', $this->text());
    }
}
