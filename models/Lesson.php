<?php

namespace app\models;

use Yii;
use understeam\calendar\ItemInterface;
use understeam\calendar\ActiveRecordItemTrait;

/**
 * This is the model class for table "lesson".
 *
 * @property int $id
 * @property string $name
 * @property string $date
 * @property string $description
 */
class Lesson extends \yii\db\ActiveRecord implements ItemInterface
{
    use ActiveRecordItemTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'lesson';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'string'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date' => 'Date',
            'description' => 'Description',
        ];
    }
}
