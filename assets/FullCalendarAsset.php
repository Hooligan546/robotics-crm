<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FullCalendarAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/fullcalendar.css',
        //'bower_components/fullcalendar/css/fullcalendar.print.css',
    ];

    public $js = [
        'js/moment.js',
        'js/fullcalendar.min.js',
        'js/calendar.js'
    ];

    public $depends = [
        'app\assets\TemplateAsset',
    ];
}
