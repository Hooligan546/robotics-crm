<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TemplateAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/template/files';

    public $css = [
        'https://fonts.googleapis.com/css?family=Roboto:400,500',
        'bower_components/bootstrap/css/bootstrap.min.css',
        'assets/pages/waves/css/waves.min.css',
        'assets/icon/themify-icons/themify-icons.css',
        'assets/icon/icofont/css/icofont.css',
        'assets/css/font-awesome.min.css',
        'assets/css/style.css',
        'assets/css/jquery.mCustomScrollbar.css',
    ];

    public $js = [
        'bower_components/jquery/js/jquery.min.js',
        'bower_components/jquery-ui/js/jquery-ui.min.js',
        'bower_components/popper.js/js/popper.min.js',
        'bower_components/bootstrap/js/bootstrap.min.js',
        'assets/pages/widget/excanvas.js',
        'assets/pages/waves/js/waves.min.js',
        'bower_components/jquery-slimscroll/js/jquery.slimscroll.js',
        'bower_components/modernizr/js/modernizr.js',
        'assets/js/SmoothScroll.js',
        'bower_components/modernizr/js/css-scrollbars.js',
        'assets/js/jquery.mCustomScrollbar.concat.min.js',
        'assets/js/pcoded.min.js',
        'assets/js/vertical/vertical-layout.min.js',
        'assets/pages/dashboard/custom-dashboard.js',
        'bower_components/sweetalert/js/sweetalert.min.js',
        'assets/js/script.js',
        'frontend-ajax.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

    /*public $publishOptions = [
        'forceCopy' => true,
    ];*/

}
