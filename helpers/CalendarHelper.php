<?php

namespace app\helpers;

class CalendarHelper
{
    /**
     * @param array $arr
     * @param string $date
     * @param string $title
     * @return string
     */
    public static function get_json($arr, $date, $title){
        $data = '[';
        foreach($arr as $key => $item){
                $data .= '{ "start": "' . $arr[$key][$date]. '", "end": "' . $arr[$key][$date]. '", "title": "' . $arr[$key][$title] . '" },';
        }
        $data .= ']';
        return $data;
    }

}