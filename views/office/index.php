<?php
/* @var $this yii\web\View */
/* @var $offices \app\models\Office */
?>
<!--  sale analytics start -->
<div class="col-xl-12 col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Список офисов</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li><i class="fa fa fa-wrench open-card-option"></i></li>
                    <li><i class="fa fa-plus-square"></i></li>
                    <li><i class="fa fa-window-maximize full-card"></i></li>
                    <li><i class="fa fa-minus minimize-card"></i></li>
                    <li><i class="fa fa-refresh reload-card"></i></li>
                    <li><i class="fa fa-trash close-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <table class="table table-hover table-borderless">
                    <thead>
                    <tr>
                        <th>Название</th>
                        <th>Описание</th>
                        <th>Адрес</th>
                        <th>Директор</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($offices as $office) {?>
                        <tr>
                            <td><?= $office->name ?></td>
                            <td><?= $office->description ?></td>
                            <td><?= $office->address ?></td>
                            <td><?= $office->director ?></td>
                            <td><i class="fa fa-pencil-square-o f-16 m-r-15 editStudent" data-id="<?=$office->id?>"></i><a href="#!"><i class="fa fa-trash f-16"></i></a></td>
                        </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<!-- row end -->
</div>
<!-- Page-body end -->
</div>
<div id="styleSelector"> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- popup edit student -->
<div id="editStudent" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>
<!-- popup edit student -->
<div id="editStudent" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
        </div>
    </div>
</div>