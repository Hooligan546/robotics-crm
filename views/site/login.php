<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-page">
    <form action="j-pro/php/action.php" method="post" class="form j-pro" id="j-pro" novalidate>
        <!-- end /.header -->
        <div class="j-content">
            <!-- start login -->
            <div class="j-unit">
                <div class="j-input">
                    <label class="j-icon-right" for="login">
                        <i class="icofont icofont-ui-user"></i>
                    </label>
                    <input type="text" id="login" name="login" placeholder="your login...">
                </div>
            </div>
            <!-- end login -->
            <!-- start password -->
            <div class="j-unit">
                <div class="j-input">
                    <label class="j-icon-right" for="password">
                        <i class="icofont icofont-lock"></i>
                    </label>
                    <input type="password" id="password" name="password" placeholder="your password...">
                    <span class="j-hint">
                        <a href="#" class="j-link">Forgot password?</a>
                    </span>
                </div>
            </div>
            <!-- end password -->
            <!-- start reCaptcha -->
            <div class="j-unit">
                <!-- start an example of the site key -->
                <div class="g-recaptcha" data-sitekey="6LeV7gwUAAAAAKOX-B12lNcg1ids8dFylMP6XihO"></div>
                <!-- end an example of the site key -->
                <!-- <div class="g-recaptcha" data-sitekey="your-site-key"></div> -->
            </div>
            <!-- end reCaptcha -->
            <!-- start response from server -->
            <div class="j-response"></div>
            <!-- end response from server -->
        </div>
        <!-- end /.content -->
        <div class="j-footer">
            <button type="submit" class="btn btn-primary">Sign in</button>
        </div>
        <!-- end /.footer -->
    </form>
</div>

