<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\models\Lesson */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>
                        <!--  sale analytics start -->
                        <div class="col-xl-12 col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Список занятий</h5>
                                    <div class="card-header-right">
                                        <ul class="list-unstyled card-option">
                                            <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                            <li><i class="fa fa-window-maximize full-card"></i></li>
                                            <li><i class="fa fa-minus minimize-card"></i></li>
                                            <li><i class="fa fa-refresh reload-card"></i></li>
                                            <li><i class="fa fa-trash close-card"></i></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-block">
                                    <div class="table-responsive">
                                        <table class="table table-hover table-borderless">
                                            <thead>
                                            <tr>
                                                <th>Имя</th>
                                                <th>Фамилия</th>
                                                <th>Отчество</th>
                                                <th>Дата рождения</th>
                                                <th>Телефон</th>
                                                <th>Имя родителя</th>
                                                <th>Фамилия родителя</th>
                                                <th>Отчество родителя</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>John Deo</td>
                                                <td>#81412314</td>
                                                <td><img src="../files/assets/images/widget/PHONE1.jpg" alt="" class="img-fluid"></td>
                                                <td>Moto G5</td>
                                                <td>10</td>
                                                <td>17-2-2017</td>
                                                <td><label class="label label-warning">Pending</label></td>
                                                <td><a href="#!"><i class="fa fa-pencil-square-o f-16 m-r-15"></i></a><a href="#!"><i class="fa fa-trash f-16"></i></a></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Добавить занятие</h5>
                                </div>
                                <div class="card-block">
                                    <?php $form = ActiveForm::begin(['action' => '/lesson/create',
                                        'options' => [
                                            'class' => 'form-material',
                                        ],
                                        'fieldConfig' => [
                                            'options' => [
                                                'tag' => false,
                                            ]
                                        ]
                                    ]); ?>

                                    <?= $form->field($model, 'name', [
                                        'template' => '<div class="form-group form-default">{input}<span class="form-bar"></span>{label}</div>'
                                    ])->input('text', ['class' => 'form-control', 'required' => true])->label('Название', ['class' => 'float-label']) ?>

                                    <?= $form->field($model, 'date', [
                                        'template' => '<div class="form-group form-default">{input}<span class="form-bar"></span>{label}</div>'
                                    ])->input('datetime-local', ['class' => 'form-control', 'required' => true])->label(false) ?>

                                    <?= $form->field($model, 'description', [
                                        'template' => '<div class="form-group form-default">{input}<span class="form-bar"></span>{label}</div>'
                                    ])->input('text', ['class' => 'form-control', 'required' => true])->label('Описание занятия', ['class' => 'float-label']) ?>

                                    <div class="form-group">
                                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                                    </div>

                                    <?php ActiveForm::end(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Page-body end -->
            </div>
            <div id="styleSelector"> </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>



