<?php

/* @var $this yii\web\View */

use app\assets\FullCalendarAsset;
use yii\widgets\ActiveForm;

FullCalendarAsset::register($this);

/* @var $lessons_json string */
/* @var $model \app\models\Lesson */
/* @var $groups \app\models\Group */

$this->title = 'My Yii Application';
?>

<script>var events = <?php echo $lessons_json ?></script>

<!--  sale analytics start -->
<div class="col-xl-12 col-md-12">
    <div class="card">
        <div class="card-header">
            <h5>Календарь расписания</h5>
            <div class="card-header-right">
                <ul class="list-unstyled card-option">
                    <li><i class="fa fa fa-wrench open-card-option"></i></li>
                    <li><i class="fa fa-window-maximize full-card"></i></li>
                    <li><i class="fa fa-minus minimize-card"></i></li>
                    <li><i class="fa fa-refresh reload-card"></i></li>
                    <li><i class="fa fa-trash close-card"></i></li>
                </ul>
            </div>
        </div>
        <div class="card-block">
            <!--<div class="col-xl-10 col-md-12">-->
                <div id='calendar'></div>
            <!--</div>-->
        </div>
    </div>
</div>
</div>
</div>
<!-- Page-body end -->
</div>
<div id="styleSelector"> </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- popup adding new event -->
<div id="addLessonForm" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php $form = ActiveForm::begin([
                'action' => '/lesson/create',
                'options' => ['class' => 'addLessonForm']
            ]); ?>
            <div class="modal-header">
                <h4 class="modal-title">Добавить занятие</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Название</label>
                    <?= $form->field($model, 'name')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
                </div>
                <div class="form-group">
                    <label>Дата</label>
                    <?= $form->field($model, 'date')->input('text', ['class' => 'form-control', 'id' => 'date', 'required' => true])->label(false) ?>
                </div>
                <div class="form-group">
                    <label>Описание</label>
                    <?= $form->field($model, 'description')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
                </div>
                <!--<div class="form-group">
                    <label>Группа</label>
                    <?/*= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map($groups, 'id', 'name'), ['class' => 'form-control form-control-default', 'required' => true])->label(false) */?>
                </div>-->
            </div>
            <div class="modal-footer">
                <input type="button" class="btn btn-default" data-dismiss="modal" value="Отмена">
                <input type="submit" class="btn btn-info" value="Сохранить">
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

