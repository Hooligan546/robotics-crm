<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model \app\models\Student */
/* @var $students \app\models\Student */
/* @var $groups array */

$this->title = 'Students';
$this->params['breadcrumbs'][] = $this->title;
?>

<!--  sale analytics start -->
                                        <div class="col-xl-12 col-md-12">
                                            <div class="card">
                                                <div class="card-header">
                                                    <h5>Список учеников</h5>
                                                    <div class="card-header-right">
                                                        <ul class="list-unstyled card-option">
                                                            <li><i class="fa fa fa-wrench open-card-option"></i></li>
                                                            <li id="add_entry" data-model="student"><i class="fa fa-plus-square"></i></li>
                                                            <li><i class="fa fa-window-maximize full-card"></i></li>
                                                            <li><i class="fa fa-minus minimize-card"></i></li>
                                                            <li><i class="fa fa-refresh reload-card"></i></li>
                                                            <li><i class="fa fa-trash close-card"></i></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="card-block">
                                                    <div class="table-responsive">
                                                        <table class="table table-hover table-borderless">
                                                            <thead>
                                                            <tr>
                                                                <th>Имя</th>
                                                                <th>Фамилия</th>
                                                                <th>Отчество</th>
                                                                <th>Дата рождения</th>
                                                                <th>Телефон</th>
                                                                <th>ФИО родителя</th>
                                                                <th>Группа</th>
                                                                <th></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <?php foreach ($students as $student) {?>
                                                            <tr>
                                                            <td><?= $student->name ?></td>
                                                            <td><?= $student->surname ?></td>
                                                            <td><?= $student->middle_name ?></td>
                                                            <td><?= $student->birthday ?></td>
                                                            <td><?= $student->phone ?></td>
                                                            <td><?= $student->parent?></td>
                                                            <td><?= $student->group->name ?></td>
                                                            <td><i class="fa fa-pencil-square-o f-16 m-r-15 editStudent" data-id="<?=$student->id?>"></i><a href="#!"><i class="fa fa-trash f-16"></i></a></td>
                                                            </tr>
                                                            <?php }?>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- row end -->
                                </div>
                                <!-- Page-body end -->
                            </div>
                            <div id="styleSelector"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- popup edit student -->
<div id="editStudent" class="modal fade">
</div>


