<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $groups app\models\Group */

$this->title = 'Update Student: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Students', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
    <?= $this->render('_form', [
        'model' => $model,
        'groups' => $groups,
    ]) ?>
