<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Student */
/* @var $groups app\models\Group */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-dialog">
    <div class="modal-content">
        <?php $form = ActiveForm::begin([
            'action' => '/student/update',
            'options' => [
                    'class' => 'lessonForm'
            ]]); ?>
        <div class="modal-header">
            <h4 class="modal-title">Редактирование</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <?= $form->field($model, 'id')->hiddenInput() ?>
                <label>Имя</label>
                <?= $form->field($model, 'name')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
            </div>
            <div class="form-group">
                <label>Фамилия</label>
                <?= $form->field($model, 'surname')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
            </div>
            <div class="form-group">
                <label>Отчество</label>
                <?= $form->field($model, 'middle_name')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
            </div>
            <div class="form-group">
                <label>Дата рождения</label>
                <?= $form->field($model, 'birthday')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
            </div>
            <div class="form-group">
                <label>Телефон</label>
                <?= $form->field($model, 'phone')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
            </div>
            <div class="form-group">
                <label>ФИО родителя</label>
                <?= $form->field($model, 'parent')->input('text', ['class' => 'form-control', 'required' => true])->label(false) ?>
            </div>
            <div class="form-group">
                <label>Группа</label>
                <?= $form->field($model, 'group_id')->dropDownList(ArrayHelper::map($groups, 'id', 'name'), ['class' => 'form-control form-control-default', 'required' => true])->label(false) ?>
            </div>
        </div>
        <div class="modal-footer">
            <input type="button" class="btn btn-default" data-dismiss="modal" value="Отмена">
            <input type="submit" class="btn btn-info" value="Сохранить">
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
